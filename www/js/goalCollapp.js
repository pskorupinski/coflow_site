
var getApp = angular.module('goalCollaboratorsapp', ['menu']);


//getApp.config(function($locationProvider) {
//    $locationProvider.html5Mode(true); 
//  });


//myApp.directive('myDirective', function() {});
//myApp.factory('myService', function() {});


getApp.controller('GoalCollaboratorsController', ['$scope', '$http', '$location', '$route', function($scope, $http, $location, $route){
    
    /*GET goal collaborators*/
    $scope.getGoalCollaborators = function(){
        console.log('hejo');
        //goaldi bierzemy z pliku addgoalapp (przy przekierowaniu na collaborators)
        //console.log($location.search());
        $scope.goalid = $location.search().goalid;
        var goalid = $scope.goalid;
        console.log(goalid);
        
        $http({
            method : 'GET',
            url    : URL_BASE + '/api/people/me/goals/' + goalid + '/collaborators'
        })
        .success(function(data){
            console.log("success");
            $scope.goalCollaborators = data;
        })
        .error(function(){
            console.error("error");
            $scope.errorMessage = 'No collaborators were found';
        })
    };
    
    /*PUT (change / update) collaborators' role*/
    $scope.changeRoles = function(){
        
        $scope.goalid = $location.search().goalid;
        var goalid = $scope.goalid;
        
        if ($scope.coll.role === undefined) {
                $scope.errorMessage = 'required';
        }
        else{
            $http({
            method : 'PUT',
            url    : URL_BASE + '/api/people/me/goals/' + goalid + '/collaborators/person_id'
            })
            .success(function(data){
                console.log('elo elo 3 5 0');
                $scope.coll.role = data.role;
                $scope.coll.isconsiliere = data.isconsiliere;
            })
            .error(function(){
                $scope.errorMessage = 'We could not change role of this collaborator';
            })
        };
        
    }
    
    /*review collaborator NIE DZIALA*/
    $scope.reviewCollaborator = function($window){
        if ($scope.coll.review === undefined){
            $scope.errorMessage = 'required';
        }
        else{
            $http({
                method : 'POST', 
                url    : URL_BASE + ''
            })
            .success(function(data){
                $scope.coll.review = data;
            })
            .error(function(){
                $scope.errorMessage = 'required';
            })
        }
    };
    
    /*NIE DZIALA*/
    $scope.reportCollaborator = function(){
        if ($scope.coll.negativeReport === undefined){
            $scope.errorMessage = 'required';
        }
        else{
            $http({
                method : 'POST',
                url    : URL_BASE + '/api/reported/me/negative-reports'
            })
            .success(function(data){
                console.log('negative report');
                $scope.coll.negativeReport = data;
            })
            .error(function(){
                $scope.errorWhy = 'required';
            })
        }
    };
    
    /*DELETE collaborator DONE*/
     $scope.deleteCollaborator = function(){ 
         
        $scope.goalid = $location.search().goalid;
        var goalid = $scope.goalid; 
         
        $http({
            method : 'DELETE',
            url    : URL_BASE + '/api/people/person_id/goals/' + goalid + '/collaborators/person_id'
        })
        .success(function(data){
            console.log('deleted');
            $scope.reloadRoute = function() {
                $route.reload();
            }
        })
        .error(function(){
            $scope.errorWhy = 'required';
        })
    };
    
}]);

getApp.controller('CandidatesController', ['$scope', '$http', '$location', function($scope, $http, $location){
     
    $scope.getCandidates = function(){
        console.log('candidates');
        $scope.goalid = $location.search().goalid;
        var goalid = $scope.goalid;
        
        $http({
            method : 'GET',
            url    : URL_BASE + '/api/people/me/goals/' + goalid + '/candidates'
        })
        .success(function(data){
            console.log('success cand');
            $scope.candidates = data;
        })
        .error(function(){
             $scope.errorMessage = 'No candidates were found';
        })

    };
    
    $scope.invite = function(){
      $http({
          method: 'POST',
          url: URL_BASE + ''
      })  
    };
}]);