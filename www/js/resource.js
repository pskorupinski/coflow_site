
(function() {
    var app;

    app = angular.module('coflow.api', ['ngResource']);

    ///////////////////////////////////////////////////

    app.factory('Character', [
        '$resource', function($resource) {
            return $resource(URL_BASE + '/api/characters/:characterid')
        }
    ]);

    app.factory('Skill', [
        '$resource', function($resource) {
            return $resource(URL_BASE + '/api/skills/:skillid')
        }
    ]);

    app.factory('Goal', [
        '$resource', function($resource) {
            return $resource(URL_BASE + '/api/goals/:goalid')
        }
    ]);

    app.factory('GoalVisibility', [
        '$resource', function($resource) {
            return $resource(URL_BASE + '/api/goals/visibilities')
        }
    ]);

    app.factory('GoalSkill', [
        '$resource', function($resource) {
            return $resource(URL_BASE + '/api/goals/:goalid/skills')
        }
    ]);

    ////////////////////////////////////////////////////

    app.factory('PersonGoal', [
        '$resource', function($resource) {
            return $resource(URL_BASE + '/api/people/:personid/goals/:goalid')
        }
    ]);

    ////////////////////////////////////////////////////

  app.factory('User', [
    '$resource', function($resource) {
      return $resource('/api/users/:username', {
        username: '@username'
      });
    }
  ]);

  app.factory('Post', [
    '$resource', function($resource) {
      return $resource('/api/posts/:id', {
        id: '@id'
      });
    }
  ]);

  app.factory('Photo', [
    '$resource', function($resource) {
      return $resource('/api/photos/:id', {
        id: '@id'
      });
    }
  ]);

  app.factory('UserPost', [
    '$resource', function($resource) {
      return $resource('/api/users/:username/posts/:id');
    }
  ]);

  app.factory('PostPhoto', [
    '$resource', function($resource) {
      return $resource('/api/posts/:post_id/photos/:id');
    }
  ]);

}).call(this);