/*created by: Kasia
 */
'use strict';

angular.module('Otd', ['menu', 'OtdDirectives']);

/* Controllers */
function SearchForm($scope, $http, $window){
    $scope.location = '';

    /*GET location*/
    $scope.getLocation = function(){
        var locationstring = "";
        $http({
            method  : 'GET',
            url     : URL_BASE + '/api/people/me/locations',
            //data    : $scope.location,
        })
        .success(function(data){
            console.log('locations on server:');
            console.log(data);
            $scope.locationstring = data[0].locationname;
        })
        .error(function(){
            console.log('error');
            $scope.errorMessage = 'required';
        })
        return locationstring;
    }
    
    $scope.doSearch = function(){
        if($scope.location === ''){
            console.log('nie ma lokalizacji');
            //alert('Directive did not update the location property in parent controller.');
            $scope.errorLocation = 'required';
        } else {
            //alert('Yay. Location: ' + $scope.location);
            $scope.submitForm = function() {
               // function(){
                console.log('jest');
                $http({
                  method  : 'POST',
                  url     : URL_BASE + '/api/people/me/locations',
//                  url : URL_BASE + '/api/people/1/goals/1',
                  data    : $scope.location,
                })
            .success(function(data, $window) {
                console.log('Dodano lokalizację');
            })
            .error(function(){
                console.log('hej hej hej ');
                $scope.errorLocation = 'required';
            })
            };
        }
    };
}

/* Directives */
angular.module('OtdDirectives', ['Otd']).
    directive('googlePlaces', function(){        
        return {
            restrict:'E',
            replace:true,
            // transclude:true,
            scope: {location:'='},
            template: '<input id="google_places_ac" name="google_places_ac" type="text" class="input-block-level text1"                                             required/>',
            link: function($scope, elm, attrs){
                var autocomplete = new google.maps.places.Autocomplete($("#google_places_ac")[0], {});
                google.maps.event.addListener(autocomplete, 'place_changed', function() {
                    var place = autocomplete.getPlace();
                    var locationObj = {locationX : place.geometry.location.lat(), 
                                       locationY : place.geometry.location.lng(),
                                       locationname : place.formatted_address};
                    $scope.location = locationObj;
                    //console.log(place.formatted_address);
                    $scope.$apply();
                });
            }
        }
    });
