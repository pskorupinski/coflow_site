//facebook here
//***************************************************
window.fbAsyncInit = function() {
    FB.init({
        appId      : '136385270053815',
        status     : true,
        xfbml      : true
    });
};

(function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s);
     js.id = id;
     js.src = "//connect.facebook.net/en_US/all.js";
     fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var fbApp = angular.module('fb', ['restangular', 'menu']);

fbApp.config(["$httpProvider",
    function (provider) {
        provider.defaults.headers.common["X-CSRFToken"] = getCookie('csrftoken');
    }
]);

fbApp.controller("UserCtrl",
	['$scope','$location', '$http', 'Facebook', 'Restangular',
        function($scope, $location, $http, Facebook, Restangular /*we will write this factory next*/){

            Restangular.setBaseUrl('http://localhost:8000/'); // 8000 to serwer API

            var accessToken = "";

            $scope.login_fb = function(){
                console.log("function login start");
                Facebook.login({scope: 'email,user_likes'}).then(function(response){
                   console.log(response);
                   accessToken = response.authResponse.accessToken;
                   $http.defaults.headers.common['Authorization'] = 'Bearer facebook ' + accessToken;
                    console.log(accessToken);

                   // TODO
                   // CALL /api/people/me/status
                   // - if fields are empty: redirect to user profile edit page
                   // - else: redirect to dashboard
                   $http({
                        method : 'GET',
                        url    : 'http://localhost:8000' + '/api/people/me/status'
                    })
                        .success(function(data){
                            console.log(data);
                        })
                        .error(function(){
                            console.error("Status API responded with error.");
                        })

                   //$http({
                   //        method : 'POST',
                   //        url    : 'http://localhost:8000' + '/auth/convert-token',
                   //        'content-type': 'application/x-www-form-urlencoded',
                   //        data   : {grant_type:'convert_token',
                   //            client_id:'ze0O70D3pkBIdrZPahvmlbebu6HqCPvwCjt6ML5l',
                   //            client_secret:'6n10diIWLUMNDB6hR5JMpQHzHQY2eIfx7XgnZYBrWOw5m9VTTUT98XH1xBpuWD4Joqlag0AfxASQrLdpLk1k94sYZAHsR17iWSnOBRafRpiPURtkN3v5W53D0y9U7ymO',
                   //            backend:'facebook',
                   //            token:accessToken}
                   //    })
                   //        .success(function(data){
                   //            console.log(data);
                   //        })
                   //        .error(function(){
                   //            console.error("error");
                   //        })

                   //var reqObj = {"access_token": response.authResponse.accessToken,
                   //           "backend": "facebook"};
                   //var u_b = Restangular.all('login/');
                   //u_b.post(reqObj).then(function(response,headers) {
                   //     $location.path('/home');
                   //    console.log(response);
                   //    console.log(headers);
                   //     //$http.defaults.headers.common['Authorization'] = 'Token ' + response.token;
                   //}, function(response) { /*error*/
                   //    console.log("There was an error", response);
                   //    //deal with error here.
                   //});
               });
            }

            $scope.get_skills = function() {
                $http({
                    method : 'GET',
                    url    : 'http://localhost:8000' + '/api/skills'
                })
                .success(function(data){
                    console.log(data);
                })
                .error(function(){
                    console.error("error");
                })
            }


}]);

fbApp.factory('Facebook',
    ["$q", "$window", "$rootScope",
    function($q, $window, $rootScope) {

	// since we are resolving a thirdparty response,
        // we need to do so in $apply
	var resolve = function(errval, retval, deferred) {
	    $rootScope.$apply(function() {
	        if (errval) {
		    deferred.reject(errval);
	        } else {
		    retval.connected = true;
	            deferred.resolve(retval);
	        }
	    });
        }

	var _login = function(){
	    var deferred = $q.defer();
            //first check if we already have logged in
	    FB.getLoginStatus(function(response) {
	        if (response.status === 'connected') {
	            // the user is logged in and has authenticated your
		    // app
		    console.log("fb user already logged in");
		    deferred.resolve(response);
		} else {
		    // the user is logged in to Facebook,
		    // but has not authenticated your app
		    FB.login(function(response){
		        if(response.authResponse){
			    console.log("fb user logged in");
			    resolve(null, response, deferred);
			}else{
			    console.log("fb user could not log in");
			    resolve(response.error, null, deferred);
			}
		    });
		 }
	     });

	     return deferred.promise;
	}

	return{
		login: _login,
	};
}]);