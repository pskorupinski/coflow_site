
coflowDev = angular.module('coflowDev', ['addGoalApp', 'ngMockE2E', 'goalCollaboratorsapp', 'goalPageapp', 'Otd']);

// define our fake backend
coflowDev.run(function($httpBackend) {


    /*
     API DEFINITION
      */

//    // GET /api/goals
//    $httpBackend.whenGET(/^http\:\/\/localhost:8000\/api\/goals$/, undefined/*headers*/)
//        .respond(getGoalsRespond);

    $httpBackend.whenGET(/^http\:\/\/localhost:8000\/api\/goals$/, undefined/*headers*/).passThrough();

    // GET /api/goals/visibilities
    $httpBackend.whenGET(/^http\:\/\/localhost:8000\/api\/goals\/visibilities$/, undefined/*headers*/)
        .respond(getVisibilitiesRespond);
    // GET /api/people/person_id/goals
    //$httpBackend.whenGET(/^http\:\/\/localhost:8000\/api\/people\/(.+)\/goals$/, undefined/*headers*/)
    //    .respond(getPersonGoalsRespond);
    $httpBackend.whenGET(/^http\:\/\/localhost:8000\/api\/people\/(.+)\/goals$/, undefined/*headers*/).passThrough();
    // GET /api/people/person_id/goals/goal_id
    // POST /api/people/person_id/goals/goal_id
    $httpBackend.whenPOST(/^http\:\/\/localhost:8000\/api\/people\/(.+)\/goals\/(.+)$/, undefined/*data*/, undefined/*headers*/)
        .respond(addPersonGoalRespond);
//    // POST /api/people/person_id/goals (deprecated)
//    $httpBackend.whenPOST(/^http\:\/\/localhost:8000\/api\/people\/(.+)\/goals\//, undefined/*data*/, undefined/*headers*/)
//        .respond(addPersonGoalRespondOld);
    $httpBackend.whenGET(/^http\:\/\/localhost:8000\/api\/people\/(.+)\/goals\/(.+)\/collaborators$/, undefined/*headers*/)
        .respond(getPersonGoalCollaboratorsRespond);

    //POST /api/reported/me/negative-reports
    $httpBackend.whenPOST(/^http\:\/\/localhost:8000\/api\/reported\/(.+)\/negative\-reports$/, undefined/*data*/, undefined/*headers*/)
        .respond(addCollaboratorsNegativeReportRespond);

    //DELETE /api/people/person_id/goals/goal_id/collaborators/person_id
    $httpBackend.whenDELETE(/^http\:\/\/localhost:8000\/api\/people\/(.+)\/goals\/(.+)\/collaborators\/(.+)$/, undefined/*data*/, undefined/*headers*/)
    .respond(deleteCollaboratorRespond);

    $httpBackend.whenGET(/^http\:\/\/localhost:8000\/api\/people\/(.+)\/goals\/(.+)\/candidates$/, undefined/*headers*/)
        .respond(getPersonGoalCandidatesRespond);

    //POST /api/people/me/locations
    $httpBackend.whenPOST(/^http\:\/\/localhost:8000\/api\/people\/(.+)\/locations$/, undefined/*data*/, undefined/*headers*/)
        .respond(addLocationRespond);    
    
    //GET /api/people/me/locations
    $httpBackend.whenGET(/^http\:\/\/localhost:8000\/api\/people\/(.+)\/locations$/, undefined/*data*/, undefined/*headers*/)
        .respond(getLocationsRespond);  
    
    $httpBackend.whenPOST(/^.+\.html/).passThrough();
    $httpBackend.whenPUT(/^.+\.html/).passThrough();
    $httpBackend.whenGET(/^.+\.html/).passThrough();


    /*
     STATIC DATA
      */

    var goals = [{id: '1', name: 'Master myself'},
        {id: '2', name: 'Share/express myself'},
        {id: '3', name: 'Organize for community'},
        {id: '4', name: 'Innovate'}, 
        {id: '5', name: 'Support others'}];

    var visibilities = [{id: '1', name: 'Let everyone nearby know!'},
        {id: '2', name: 'Let only my collaborators see'},
        {id: '3', name: 'Let only this mission\'s collaborators see'}];

    var collaborators = [];
    var candidates = [];
    
    var locations = [];
    
    locations.push({locationX : 50.06465009999999, locationY :19.94497990000002, locationname:"Kraków, Poland"});
    
    collaborators.push( { person: {personid: 5, firstname: 'Kasia', lastname: 'Zuki', weeklyhours: 4},
                       matchingSkills: [{personid: 5, name: 'Fitness', level: 90},
                                       {personid: 5, name: 'Reading', level: 20}],
                       matchingGoals: [{personid: 5, name: 'Create professional website'}],
                       role: 'Guru', iscounselor: true, location: 'krk' , contact: '456780356'} );
    
    collaborators.push( { person: {personid: 6, firstname: 'Filip', lastname: 'Turczynowicz', weeklyhours: 20},
                       matchingSkills: [{personid: 6, name: 'Programming languages', level: 90}],
                       matchingGoals: [{personid: 6, name: 'Create professional website'}],
                       role: 'Fellow', iscounselor: false, location: 'krk', contact: '7834568495' } );
    
    candidates.push( { person: {personid: 7, firstname: 'Ola', lastname: 'XX', weeklyhours: 20},
                       matchingSkills: [{personid: 7, name: 'Programming languages', level: 90}, 
                                       {personid: 7, name: 'French', level: 90}, 
                                       {personid: 7, name: 'Reading', level: 90}],
                       matchingGoals: [{personid: 7, name: 'Create professional website'},
                                      {personid: 7, name: 'Run a marathon'}],
                       location: 'krk'} );

    /*
     SIMULATION METHODS
      */

    var meGoals = [];

    meGoals.push( { personid:1, goalid:1, what:'I would like to organize gymnastics camps in the forest',
        why:'That would be a lot of fun and I could teach people a lot over there', deadline:'2016-12-31', visibility:0 } );
    
    meGoals.push( { personid:1, goalid:2, what:'Build a treehouse',
        why:'blablabla', deadline:'2017-12-01', visibility:0 } );

    function getGoalsRespond(method, url, data, headers, params) {
        var res = [200, goals];
        return res;
    }

    function getVisibilitiesRespond(method, url, data, headers, params) {
        var res = [200, visibilities];
        return res;
    }

    function getPersonGoalsRespond(method, url, data, headers, params) {
        console.log('meGoals');
        console.warn(locations);
        var res = [200, meGoals];
        return res;
    }

    function getPersonGoalRespond(method, url, data, headers, params) {
        var personid = params.personid;
        var goalid   = params.goalid;

        if(personid === 'me')
            return [200, meGoals[Number(goalid)]];
    }

    function addPersonGoalRespond(method, url, data, headers, params) {
        console.log("addPersonGoalRespond");
        console.log("url");
        console.log(url);
        console.log("data");
        console.log(data);
        data = angular.fromJson(data);

        console.log(data.what);

        // If required not in - return error
        var absentRequired = [];
        if(data.what === undefined) 
        { 
            console.log('wtf1'); 
            absentRequired.push('what'); 
        }
        if(data.visibility === undefined) 
        { 
            console.log('wtf2');  
            absentRequired.push('visibility') 
        };
        if(absentRequired.length>0)
            return[400, absentRequired];

        // TODO if goal exists - return error
        var paramsarray = getParamsArray(url);
        var goalid = getParamValue(paramsarray,'goals');
        if (meGoals[0].goalid === goalid)
        {
            return[409];
        }

        return [201, "added"];
    }

    function addPersonGoalRespondOld(method, url, data, headers, params) {
        console.log(url);
        console.log(data);
        console.log(params);

        var paramsarray = getParamsArray(url);

        var personid = getParamValue(paramsarray,'people');
        console.log("personid: "+personid);

        return [201, "added"];
    }
    
    function addCollaboratorsNegativeReportRespond(method, url, data, headers, params){
        console.log("added nR");
        return [201, "added negative report"];
    }
    
    function getPersonGoalCollaboratorsRespond(method, url, data, headers, params){
        console.log('siemka');
        var res = [200, collaborators];
        return res;
    }
    
    function getPersonGoalCandidatesRespond(method, url, data, headers, params){
        console.log('resCand');
        var res = [200, candidates];
        return res;
    }
    
    function deleteCollaboratorRespond(method, url, data, headers, params){
        var res = [200, collaborators];
        return res;
    }
    
    function addLocationRespond(method, url, data, headers, params){
        locations.push(data);
        console.warn(locations);
        var res = [201, locations];
        return res;
    }
    
    function getLocationsRespond(method, url, data, headers, params){
        console.warn(locations);
        var res = [200, locations];
        return res;
    }


    /*
     EXAMPLE CODE
      */

    //var phones = [{name: 'phone1'}, {name: 'phone2'}];
    //
    //$httpBackend.whenPOST('/phones').respond(function(method, url, data, headers){
    //  console.log('Received these data:', method, url, data, headers);
    //  phones.push(angular.fromJson(data));
    //  return [200, {}, {}];
    //});
    //
    //$httpBackend.whenGET('/phones').respond(function(method,url,data) {
    //  console.log("Getting phones");
    //  return [200, phones, {}];
    //});


    /*
     USEFUL METHODS
      */

    /**
     * Function splitting url into array
     *
     * @param url
     * @returns {Array}
     */
    function getParamsArray(url) {
        return url.split('/');
    }

    /**
     * Function returning value of parameter defined in URI
     *
     * @param urlparts
     * @param param
     * @returns {*}
     */
    function getParamValue(urlparts,param) {
        var index = urlparts.indexOf(param);
        return urlparts = urlparts[index+1];
    }

});