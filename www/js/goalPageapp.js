var getApp = angular.module('goalPageapp', ['menu']);

getApp.controller('GoalPageController', ['$scope', '$http', '$location', function($scope, $http, $location){
   
    $scope.getGoalData = function(){
        
        $http({
            method : 'GET',
            url    : URL_BASE + '/api/people/1/goals'
        })
        .success(function(data){
            console.log("success");
            $scope.meGoals = data;
        })
        .error(function(){
            console.error("error");
            $scope.errorMessage = 'No goals were found';
        })
    };
    
    $scope.sortByDeadline = function(){
        
    }
    
    
}]);