#!/usr/bin/env python

import requests
import unittest

from e2ecommon import SERVER_URL, AuthTester


class BasicApiTest(unittest.TestCase):
    def test_if_get_api_versions_works(self):
        result = requests.get(SERVER_URL + "/api/versions/")
        self.assertEqual(result.status_code, 200)
        self.assertIn("versions", result.json())

    def test_if_get_api_versions_returns_any_version(self):
        result = requests.get(SERVER_URL + "/api/versions/")
        versions = result.json()['versions']
        self.assertGreater(len(versions), 0)

    def test_if_each_api_returns_its_version(self):
        result = requests.get(SERVER_URL + "/api/versions/")
        versions = result.json()['versions']
        for version in versions:
            result = requests.get(SERVER_URL + version['url'] + 'version/')
            self.assertEqual(result.status_code, 200)

    def test_if_each_api_returns_its_version_correctly(self):
        result = requests.get(SERVER_URL + "/api/versions/")
        versions = result.json()['versions']
        for version in versions:
            result = requests.get(SERVER_URL + version['url'] + 'version/')
            retrived_version = result.json()
            self.assertEqual(version, retrived_version)


class LoginTest(unittest.TestCase):
    def test_if_login_with_email_and_password_works(self):
        auth = AuthTester("email")
        self.assertIsNotNone(auth.token)
        self.assertIsNotNone(auth.token_expiration)
        self.assertIsNotNone(auth.token_to_exchange)

    def _test_if_login_with_facebook_works(self):
        auth = AuthTester("facebook")
        self.assertIsNotNone(auth.token)
        self.assertIsNotNone(auth.token_expiration)
        self.assertIsNotNone(auth.token_to_exchange)

    def test_if_exchanging_token_works(self):
        auth = AuthTester("email")
        token, expiration, exchange = auth.token, auth.token_expiration, auth.token_to_exchange
        auth.exchange_token()
        self.assertIsNotNone(auth.token)
        self.assertIsNotNone(auth.token_expiration)
        self.assertIsNotNone(auth.token_to_exchange)
        self.assertNotEqual(token, auth.token)
        self.assertNotEqual(expiration, auth.token_expiration)
        self.assertNotEqual(exchange, auth.token_to_exchange)


if __name__ == '__main__':
    unittest.main()
