
import os
import requests
from base64 import b64encode
from unittest import TestCase


SERVER_URL = 'http://localhost:8000'
API_CALLS_ENDPOINT = "/auth/"
API_VERSION = "1.0"
CONVERT_TOKEN_PATH = "convert-token/"
RENEW_TOKEN_PATH = "token/"

CLIENT_ID = "0pQseiObVoUZV8JOHeVWCySM07eLdBWq080tACFl"
CLIENT_SECRET = "PTcdJzU37fiFT59oguoko9cvGNulkzHqGpy0Emoe2bmTomBhMSIzTtX1TXEPDJY8v1cs7muaeqmyjTc5aZzgoA0FqijGyyHYc9c7zQUyDgHVkxzgJ8myw8De9iLHqGi8"
FB_TOKEN = ""


TEST_EMAIL = "test@test.pl"
TEST_PASSWORD = "test1234"


def run_test_setup():
    os.system("python ../src/manage.py e2e_test_data")


def get_api_path(path):
    return SERVER_URL + "/api/%s/%s"%(API_VERSION, path)


class AuthTester(object):
    def __init__(self, login_type):
        assert login_type in ('email', 'facebook')
        self.token = None
        self.token_expiration = None
        self.token_to_exchange = None

        if login_type == "email":
            self._login_with_email()
        if login_type == "facebook":
            self._login_with_fb()

    def _login_with_email(self):
        email_pass_raw = "%s:%s"%(TEST_EMAIL,TEST_PASSWORD)
        email_pass_b64 = b64encode(email_pass_raw)
        auth = b64encode("%s:%s"%(CLIENT_ID,CLIENT_SECRET))
        url = SERVER_URL + API_CALLS_ENDPOINT + CONVERT_TOKEN_PATH
        headers = {'Authorization': "basic " + auth, 'Content-Type': 'application/x-www-form-urlencoded'}
        data = {'grant_type':'password'}
        print url
        print headers
        print data
        result = requests.post(url, headers=headers,data=data)
        parsed = result.json()
        print parsed
        self.token = parsed.get('access_token')
        self.token_expiration = parsed.get('expires_in', None)
        self.token_to_exchange = parsed.get('refresh_token', None)

    def _login_with_fb(self):
        headers = {'Authorization': "Bearer facebook " + FB_TOKEN}
        result = requests.get(SERVER_URL + API_CALLS_ENDPOINT + CONVERT_TOKEN_PATH, headers=headers)
        parsed = result.json()
        self.token = parsed.get('access_token')
        self.token_expiration = parsed.get('expires_in', None)
        self.token_to_exchange = parsed.get('refresh_token', None)

    def exchange_token(self):
        renew_data = {
            'grant_type': 'refresh_token',
            'client_id': CLIENT_ID,
            'client_secret': CLIENT_SECRET,
            'refresh_token': self.token_to_exchange,
        }
        result = requests.post(SERVER_URL + API_CALLS_ENDPOINT + RENEW_TOKEN_PATH, renew_data)
        parsed = result.json()
        self.token = parsed.get('access_token')
        self.token_expiration = parsed.get('expires_in', None)
        self.token_to_exchange = parsed.get('refresh_token', None)

    def get_headers(self):
        assert self.token
        return {'Authorization': "bearer " + self.token}


auth = AuthTester('email')

class EnhancedTestCase(TestCase):
    run_setup = True

    def assertEqualWithExceptions(self, obj1, obj2, varying_keys=None, extra_keys=None):
        if varying_keys is None:
            varying_keys = []
        if  extra_keys is None:
            extra_keys = []
        self.assertEqual(set(obj1.keys()) - set(extra_keys), set(obj2.keys()))
        for key in obj2:
            if key not in varying_keys:
                if obj1[key].__class__ == list and obj2[key].__class__ == list:
                    self.assertEqual(set(obj1[key]), set(obj2[key]), key)
                else:
                    self.assertEqual(obj1[key], obj2[key], "%s: %s != %s"%(key, obj1[key], obj2[key]))
            else:
                self.assertIsNotNone(obj1[key], key)

    def setUp(self):
        self.headers = auth.get_headers()
        if self.run_setup:
            run_test_setup()
