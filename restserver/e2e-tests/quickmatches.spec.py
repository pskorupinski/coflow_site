#!/usr/bin/env python

import requests
import unittest

from e2ecommon import EnhancedTestCase, get_api_path


PROFILE_KEYS = set([u'pk', u'firstName', u'gender', u'location', u'countryId', u'latitude', u'longitude', u'birthDate',
    u'birthDateVisible', u'lastActive', u'avatarUrl', u'avatarSmallUrl'])

QUICK_MATCH_VOTE_YES = {'votedId': 91, 'vote': 1 }
QUICK_MATCH_VOTE_NO = {'votedId': 91, 'vote': 3 }
QUICK_MATCH_VOTE_MUTUAL = {'votedId': 92, 'vote': 1 }

QUICK_MATCH_VOTE_YES_RESPONSE = {
    'pk': 'whatever',
    'voterId': 193,
    'votedId': 91,
    'vote': 1,
    'mutual': False,
    'date': 'whatever'
}

QUICK_MATCH_VOTE_NO_RESPONSE = {
    'pk': 'whatever',
    'voterId': 193,
    'votedId': 91,
    'vote': 3,
    'mutual': False,
    'date': 'whatever'
}

QUICK_MATCH_VOTE_MUTUAL_RESPONSE = {
    'pk': 'whatever',
    'voterId': 193,
    'votedId': 92,
    'vote': 1,
    'mutual': True,
    'date': 'whatever'
}


class QuickmatchesTest(EnhancedTestCase):
    run_setup = False

    def test_get_quickmatches(self):
        response = requests.get(get_api_path("quickmatches"), headers=self.headers)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(len(data['ids']), 20)
        for profile in data['profiles']:
            self.assertEqual(set(profile.keys()), PROFILE_KEYS)

    def test_get_10_quickmatches(self):
        response = requests.get(get_api_path("quickmatches?number=10"), headers=self.headers)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(len(data['ids']), 10)
        for profile in data['profiles']:
            self.assertEqual(set(profile.keys()), PROFILE_KEYS)

    def test_get_quickmatches_with_exclude_ids(self):
        response = requests.get(get_api_path("quickmatches"), headers=self.headers)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(len(data['ids']), 20)
        exclude_ids = data['ids']
        exclude_ids_str = ",".join(map(str, exclude_ids))
        response = requests.get(get_api_path("quickmatches?exclude=%s"%exclude_ids_str), headers=self.headers)
        data = response.json()
        new_ids = data['ids']
        self.assertEqual(len(new_ids), 20)
        self.assertEqual(len(set(exclude_ids) & set(new_ids)), 0)

    def test_vote_yes(self):
        response = requests.post(get_api_path("quickmatches/vote"), json=QUICK_MATCH_VOTE_YES, headers=self.headers)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqualWithExceptions(data, QUICK_MATCH_VOTE_YES_RESPONSE, ('date', 'pk'))

    def test_vote_no(self):
        response = requests.post(get_api_path("quickmatches/vote"), json=QUICK_MATCH_VOTE_NO, headers=self.headers)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqualWithExceptions(data, QUICK_MATCH_VOTE_NO_RESPONSE, ('date', 'pk'))

    def test_vote_yes_mutual(self):
        response = requests.post(get_api_path("quickmatches/vote"), json=QUICK_MATCH_VOTE_MUTUAL, headers=self.headers)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqualWithExceptions(data, QUICK_MATCH_VOTE_MUTUAL_RESPONSE, ('date', 'pk'))


if __name__ == '__main__':
    unittest.main()
