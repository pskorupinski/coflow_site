#!/usr/bin/env python

import requests
import unittest

from e2ecommon import EnhancedTestCase, get_api_path

PHOTO_RESPONSE = {'url': 'whatever', 'pk': 'whatever'}
PHOTO_UPLOAD_RESPONSE = {'url': 'whatever', 'pk': 'whatever', 'isAvatar': False}
AVATAR_UPLOAD_RESPONSE = {'url': 'whatever', 'smallUrl': 'whatever', 'pk': 'whatever', 'isAvatar': True}
OTHER_USER_PK = 91
DEFAULT_AVATAR_RESPONSE = {'avatarUrl': None, 'avatarSmallUrl': None}
REPLACED_AVATAR_RESPONSE = {'avatarUrl': 'whatever', 'avatarSmallUrl': 'whatever'}
SET_AS_AVATAR_RESPONSE = {
    'avatar': REPLACED_AVATAR_RESPONSE,
    'photo': PHOTO_RESPONSE
}

class PhotosTest(EnhancedTestCase):
    def test_post_photo(self):
        files = {'media': open('test.jpg', 'rb')}
        response = requests.post(get_api_path('user/photos'), files=files, headers=self.headers)
        self.assertEqual(response.status_code, 201)
        self.assertEqualWithExceptions(response.json(), PHOTO_UPLOAD_RESPONSE, ('pk', 'url'))

    def test_post_photo_other(self):
        files = {'media': open('test.jpg', 'rb')}
        response = requests.post(get_api_path('user/%d/photos'%OTHER_USER_PK), files=files, headers=self.headers)
        self.assertEqual(response.status_code, 403)

    def test_delete_photo(self):
        files = {'media': open('test.jpg', 'rb')}
        response = requests.post(get_api_path('user/photos'), files=files, headers=self.headers)
        self.assertEqual(response.status_code, 201)
        self.assertEqualWithExceptions(response.json(), PHOTO_UPLOAD_RESPONSE, ('pk', 'url'))
        response = requests.delete(get_api_path('user/photos/%d'%response.json()['pk']), headers=self.headers)
        self.assertEqual(response.status_code, 200)

    def test_delete_other_photo(self):
        response = requests.get(get_api_path('user/%d/photos'%OTHER_USER_PK), headers=self.headers)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertGreater(len(data), 0)
        photo_pk = data[0]['pk']
        response = requests.delete(get_api_path('user/%d/photos/%d'%(OTHER_USER_PK, photo_pk)), headers=self.headers)
        self.assertEqual(response.status_code, 404)

    def test_delete_other_photo_2(self):
        response = requests.get(get_api_path('user/%d/photos'%OTHER_USER_PK), headers=self.headers)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertGreater(len(data), 0)
        photo_pk = data[0]['pk']
        response = requests.delete(get_api_path('user/photos/%d'%(photo_pk)), headers=self.headers)
        self.assertEqual(response.status_code, 400)

    def test_set_as_avatar(self):
        response = requests.get(get_api_path('user?extra=photos'), headers=self.headers)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(len(data['photos']), 2)

        response = requests.put(get_api_path('user/photos/%d'%(data['photos'][0]['pk'])), data={'setAsAvatar': True},
            headers=self.headers)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(data.keys(), SET_AS_AVATAR_RESPONSE.keys())
        self.assertEqualWithExceptions(data['avatar'], SET_AS_AVATAR_RESPONSE['avatar'], ('avatarUrl', 'avatarSmallUrl'))
        self.assertEqualWithExceptions(data['photo'], SET_AS_AVATAR_RESPONSE['photo'], ('pk', 'url'))

    def test_set_as_avatar_with_empty_avatar(self):
        # set up - delete avatar, get photo key
        response = requests.delete(get_api_path('user/photos'), headers=self.headers)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), DEFAULT_AVATAR_RESPONSE)

        # test
        response = requests.get(get_api_path('user?extra=photos'), headers=self.headers)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(len(data['photos']), 2)

        response = requests.put(get_api_path('user/photos/%d'%(data['photos'][0]['pk'])), data={'setAsAvatar': True},
            headers=self.headers)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(data.keys(), [u"avatar"])
        self.assertEqualWithExceptions(data['avatar'], SET_AS_AVATAR_RESPONSE['avatar'], ('avatarUrl', 'avatarSmallUrl'))

        # cleanup
        files = {'media': open('test.jpg', 'rb')}
        response = requests.post(get_api_path('user/photos'), files=files, headers=self.headers)
        self.assertEqual(response.status_code, 201)
        self.assertEqualWithExceptions(response.json(), PHOTO_UPLOAD_RESPONSE, ('pk', 'url'))

    def test_delete_and_set_avatar(self):
        response = requests.delete(get_api_path('user/photos'), headers=self.headers)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), DEFAULT_AVATAR_RESPONSE)
        files = {'media': open('test.jpg', 'rb')}
        response = requests.post(get_api_path('user/photos'), files=files, headers=self.headers)
        self.assertEqual(response.status_code, 201)
        self.assertEqualWithExceptions(response.json(), AVATAR_UPLOAD_RESPONSE, ('pk', 'url', 'smallUrl'))

    def test_delete_avatar_with_replacing_it_with_photo(self):
        # setup
        response = requests.get(get_api_path('user?extra=photos'), headers=self.headers)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(len(data['photos']), 2)

        # test
        response = requests.delete(get_api_path('user/photos?replace_with=%d'%(data['photos'][0]['pk'])),
            headers=self.headers)
        self.assertEqual(response.status_code, 200)
        self.assertEqualWithExceptions(response.json(), REPLACED_AVATAR_RESPONSE, ('avatarUrl', 'avatarSmallUrl'))

        # cleanup
        files = {'media': open('test.jpg', 'rb')}
        response = requests.post(get_api_path('user/photos'), files=files, headers=self.headers)
        self.assertEqual(response.status_code, 201)
        self.assertEqualWithExceptions(response.json(), PHOTO_UPLOAD_RESPONSE, ('pk', 'url'))


if __name__ == '__main__':
    unittest.main()
