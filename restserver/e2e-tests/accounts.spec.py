
import requests
import unittest

from e2ecommon import EnhancedTestCase, get_api_path

MY_USER = {
    'pk': 'whatever',
    'firstName': 'MyTest',
    'gender': 'M',
    'location': 'Barcelona',
    'countryId': 'whatever',
    'latitude': 66.6,
    'longitude': 6.66,
    'birthDate': '1980-07-01',
    'birthDateVisible': False,
    'lastActive': 'whatever',
    'avatarUrl': 'whatever',
    'avatarSmallUrl': 'whatever'
}

MY_USER_LF = {
    'lookingFor': [1, 4, 8],
    'gender': 'F',
    'distance': 100,
    'ageFrom': 18,
    'ageTo': 40,
    'heightFrom': 160,
    'heightTo': 191,
    'acceptsPartnerWithChildren': 1
}

MY_USER_PERSONAL = {
    'height': 181,
    'bodyType': 3,
    'religion': 5,
    'politics': 3,
    'ethnicity': 1,
    'hasChildren': 1,
    'wantsChildren': 2,
    'smokes': 4,
    'aboutMe': "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis quasi quibusdam repudiandae. Neque, rerum, est. Voluptatum illo aliquid porro debitis accusantium nulla, dolores, nam ipsa culpa velit dolore doloremque asperiores.",
    'aboutMeHeadline': "The best test user you'll ever find",
    'relationshipStatus': 6,
    'languages': [4, 16],
    'interests': [22, 17, 11, 2],
    'sports': [],
    'musics': [],
    'customActivities': '',
    'customInterests': 'some custom interests, all very interesting and fascinating and so on',
    'customMusic': '',
    'customBooks': 'Terry Pratchett, Andrzej Sapkowski, Tim Ferris, Charles Munger, Neomi Klein, Wiktor Suworow',
    'customMovies': '',
    'fieldOfWork': 45, # 69
    'fieldOfStudy': 44, # 54
    'educationLevel': 2, # 3
    'workStatus': 2 # 1

}

MY_USER_PHOTOS = [{'url': 'whatever', 'pk': 'whatever'}, {'url': 'whatever', 'pk': 'whatever'}]

OTHER_USER = {
    'pk': 91,
    'firstName': 'Sadia',
    'gender': 'F',
    'location': 'Paris',
    'countryId': 78,
    'latitude': 6.66,
    'longitude': 66.6,
    'birthDate': '1981-04-07',
    'birthDateVisible': True,
    'lastActive': 'whatever',
    'avatarUrl': 'whatever',
    'avatarSmallUrl': 'whatever'
}

OTHER_USER_LF = {
    'lookingFor': [2],
    'gender': 'A',
    'distance': 100,
    'ageFrom': 18,
    'ageTo': 99,
    'heightFrom': 153,
    'heightTo': 213,
    'acceptsPartnerWithChildren': 2
}

OTHER_USER_PERSONAL = {
    'height': 163,
    'bodyType': 4,
    'religion': 6,
    'politics': 1,
    'ethnicity': 8,
    'hasChildren': 0,
    'wantsChildren': 1,
    'smokes': 3,
    'aboutMe': "Some short description of a cool test user",
    'aboutMeHeadline': "The best test user you'll ever find",
    'relationshipStatus': 2,
    'languages': [],
    'interests': [],
    'sports': [3, 1, 23, 27, 22, 26, 8],
    'musics': [20, 1, 11, 17, 12, 2, 6, 13, 8, 9, 3, 15, 14, 7, 4],
    'customActivities': 'stuff I want to do in my free time and things',
    'customInterests': '',
    'customMusic': 'the cool music I\'m listening to - so hipster it was not on the list',
    'customBooks': '',
    'customMovies': 'Korwin - The Movie, Marsjanin, Interstellar, Gwiezdne Wojny - przebudzenie szmocy',
    'fieldOfWork': 69,
    'fieldOfStudy': 54,
    'educationLevel': 3,
    'workStatus': 1

}

OTHER_USER_PHOTOS = [{'url': 'whatever', 'pk': 'whatever'}]

EMPTY_USER = {
    'pk': 92,
    'firstName': '',
    'gender': 'F',
    'location': '',
    'countryId': None,
    'latitude': None,
    'longitude': None,
    'birthDate': None,
    'birthDateVisible': False,
    'lastActive': None,
    'avatarUrl': None,
    'avatarSmallUrl': None
}

EMPTY_USER_LF = {
    'lookingFor': [],
    'gender': 'M',
    'distance': 0,
    'ageFrom': None,
    'ageTo': None,
    'heightFrom': 153,
    'heightTo': 213,
    'acceptsPartnerWithChildren': None
}

EMPTY_USER_PERSONAL = {
    'height': None,
    'bodyType': None,
    'religion': None,
    'politics': None,
    'ethnicity': None,
    'hasChildren': None,
    'wantsChildren': None,
    'smokes': None,
    'aboutMe': '',
    'aboutMeHeadline': '',
    'relationshipStatus': None,
    'languages': [],
    'interests': [],
    'sports': [],
    'musics': [],
    'customActivities': None,
    'customInterests': None,
    'customMusic': None,
    'customBooks': None,
    'customMovies': None,
    'fieldOfWork': None,
    'fieldOfStudy': None,
    'educationLevel': None,
    'workStatus': None
}

EMPTY_USER_PHOTOS = []


class AccountsTest(EnhancedTestCase):
    def test_get_user(self):
        response = requests.get(get_api_path('user'), headers=self.headers)
        self.assertEqual(response.status_code, 200)
        self.assertEqualWithExceptions(response.json(), MY_USER, ('pk', 'countryId', 'lastActive', 'avatarUrl',
            'avatarSmallUrl'))

    def test_get_lookingfor(self):
        response = requests.get(get_api_path('user/looking_for'), headers=self.headers)
        self.assertEqual(response.status_code, 200)
        self.assertEqualWithExceptions(response.json(), MY_USER_LF)

    def test_get_user_with_lookingfor(self):
        response = requests.get(get_api_path('user?extra=looking_for'), headers=self.headers)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqualWithExceptions(data, MY_USER, ('pk', 'countryId', 'lastActive', 'avatarUrl',
            'avatarSmallUrl'), ('lookingFor', ))
        self.assertEqualWithExceptions(data['lookingFor'], MY_USER_LF)

    def test_get_personal_info(self):
        response = requests.get(get_api_path('user/personal'), headers=self.headers)
        self.assertEqual(response.status_code, 200)
        self.assertEqualWithExceptions(response.json(), MY_USER_PERSONAL)

    def test_get_user_with_personal_info(self):
        response = requests.get(get_api_path('user?extra=personal_info'), headers=self.headers)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqualWithExceptions(data, MY_USER, ('pk', 'countryId', 'lastActive', 'avatarUrl',
            'avatarSmallUrl'), ('personalInfo', ))
        self.assertEqualWithExceptions(data['personalInfo'], MY_USER_PERSONAL)

    # #++ FOR LATER
    # # def test_get_quizes(self):
    # #     pass

    # # def test_get_user_with_quizes(self):
    # #     pass
    # #-- FOR LATER

    def test_get_photos(self):
        response = requests.get(get_api_path('user/photos'), headers=self.headers)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(len(data), len(MY_USER_PHOTOS))
        for i in range(0, len(data)):
            self.assertEqualWithExceptions(data[i], MY_USER_PHOTOS[i], ('url', 'pk'))

    def test_get_user_with_photos(self):
        response = requests.get(get_api_path('user?extra=photos'), headers=self.headers)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqualWithExceptions(data, MY_USER, ('pk', 'countryId', 'lastActive', 'avatarUrl',
            'avatarSmallUrl'), ('photos', ))
        self.assertEqual(len(data['photos']), len(MY_USER_PHOTOS))
        for i in range(0, len(data['photos'])):
            self.assertEqualWithExceptions(data['photos'][i], MY_USER_PHOTOS[i], ('url', 'pk'))

    def test_set_user(self):
        response = requests.put(get_api_path('user'), data=OTHER_USER, headers=self.headers)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqualWithExceptions(data, OTHER_USER, ('pk', 'lastActive', 'avatarUrl', 'avatarSmallUrl'))
        self.assertNotEqual(data, OTHER_USER['pk'])

    def test_set_lookingfor(self):
        response = requests.put(get_api_path('user/looking_for'), data=OTHER_USER_LF, headers=self.headers)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqualWithExceptions(data, OTHER_USER_LF)

    def test_set_personal_info(self):
        response = requests.put(get_api_path('user/personal'), data=OTHER_USER_PERSONAL, headers=self.headers)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqualWithExceptions(data, OTHER_USER_PERSONAL)

    def test_get_other_user(self):
        response = requests.get(get_api_path('user/%d'%OTHER_USER['pk']), headers=self.headers)
        self.assertEqual(response.status_code, 200)
        self.assertEqualWithExceptions(response.json(), OTHER_USER, ('countryId', 'lastActive', 'avatarUrl',
            'avatarSmallUrl'))

    def test_get_other_lookingfor(self):
        response = requests.get(get_api_path('user/%d/looking_for'%OTHER_USER['pk']), headers=self.headers)
        self.assertEqual(response.status_code, 200)
        self.assertEqualWithExceptions(response.json(), OTHER_USER_LF)

    def test_get_other_user_with_lookingfor(self):
        response = requests.get(get_api_path('user/%d?extra=looking_for'%OTHER_USER['pk']), headers=self.headers)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqualWithExceptions(data, OTHER_USER, ('pk', 'countryId', 'lastActive', 'avatarUrl',
            'avatarSmallUrl'), ('lookingFor', ))
        self.assertEqualWithExceptions(data['lookingFor'], OTHER_USER_LF)

    def test_get_other_personal_info(self):
        response = requests.get(get_api_path('user/%d/personal'%OTHER_USER['pk']), headers=self.headers)
        self.assertEqual(response.status_code, 200)
        self.assertEqualWithExceptions(response.json(), OTHER_USER_PERSONAL)

    def test_get_other_user_with_personal_info(self):
        response = requests.get(get_api_path('user/%d?extra=personal_info'%OTHER_USER['pk']), headers=self.headers)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqualWithExceptions(data, OTHER_USER, ('pk', 'countryId', 'lastActive', 'avatarUrl',
            'avatarSmallUrl'), ('personalInfo', ))
        self.assertEqualWithExceptions(data['personalInfo'], OTHER_USER_PERSONAL)

    # #++ FOR LATER
    # # def test_get_other_quizes(self):
    # #     pass

    # # def test_get_other_user_with_quizes(self):
    # #     pass
    # #-- FOR LATER

    def test_get_other_photos(self):
        response = requests.get(get_api_path('user/%d/photos'%OTHER_USER['pk']), headers=self.headers)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(len(data), len(OTHER_USER_PHOTOS))
        for i in range(0, len(data)):
            self.assertEqualWithExceptions(data[i], OTHER_USER_PHOTOS[i], ('url', 'pk'))

    def test_get_other_user_with_photos(self):
        response = requests.get(get_api_path('user/%d?extra=photos'%OTHER_USER['pk']), headers=self.headers)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqualWithExceptions(data, OTHER_USER, ('pk', 'countryId', 'lastActive', 'avatarUrl',
            'avatarSmallUrl'), ('photos', ))
        self.assertEqual(len(data['photos']), len(OTHER_USER_PHOTOS))
        for i in range(0, len(data['photos'])):
            self.assertEqualWithExceptions(data['photos'][i], OTHER_USER_PHOTOS[i], ('url', 'pk'))

    def test_set_other_user(self):
        response = requests.put(get_api_path('user/%d'%OTHER_USER['pk']), data={}, headers=self.headers)
        self.assertEqual(response.status_code, 403)

    def test_set_other_lookingfor(self):
        response = requests.put(get_api_path('user/%d/looking_for'%OTHER_USER['pk']), data={}, headers=self.headers)
        self.assertEqual(response.status_code, 403)

    def test_set_other_personal_info(self):
        response = requests.put(get_api_path('user/%d/personal'%OTHER_USER['pk']), data={}, headers=self.headers)
        self.assertEqual(response.status_code, 403)

    # empty user tests - getter first
    def test_get_empty_user(self):
        response = requests.get(get_api_path('user/%d'%EMPTY_USER['pk']), headers=self.headers)
        self.assertEqual(response.status_code, 200)
        self.assertEqualWithExceptions(response.json(), EMPTY_USER)

    def test_get_empty_lookingfor(self):
        response = requests.get(get_api_path('user/%d/looking_for'%EMPTY_USER['pk']), headers=self.headers)
        self.assertEqual(response.status_code, 200)
        self.assertEqualWithExceptions(response.json(), EMPTY_USER_LF)

    def test_get_empty_personal_info(self):
        response = requests.get(get_api_path('user/%d/personal'%EMPTY_USER['pk']), headers=self.headers)
        self.assertEqual(response.status_code, 200)
        self.assertEqualWithExceptions(response.json(), EMPTY_USER_PERSONAL)

    def test_set_user_empty(self):
        response = requests.put(get_api_path('user'), json=EMPTY_USER, headers=self.headers)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqualWithExceptions(data, EMPTY_USER, ('pk', 'lastActive', 'avatarUrl', 'avatarSmallUrl'))
        self.assertNotEqual(data, EMPTY_USER['pk'])

    def test_set_lookingfor_empty(self):
        response = requests.put(get_api_path('user/looking_for'), json=EMPTY_USER_LF, headers=self.headers)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqualWithExceptions(data, EMPTY_USER_LF)

    def test_set_personal_info_empty(self):
        response = requests.put(get_api_path('user/personal'), json=EMPTY_USER_PERSONAL, headers=self.headers)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqualWithExceptions(data, EMPTY_USER_PERSONAL)


if __name__ == '__main__':
    unittest.main()
