#!/usr/bin/env python
import os
import sys

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

sys.path.insert(0, os.path.join(PROJECT_ROOT, "apps"))
sys.path.insert(0, os.path.join(PROJECT_ROOT, "../lib"))

if __name__ == "__main__":
    import django

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
    django.setup()
    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
