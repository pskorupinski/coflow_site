"""coflow URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from prototype.views.dictionaries import *

characters = [
    url(r'^$', CharacterList.as_view()),
    url(r'^(?P<id>[0-9]+)/$', CharacterId.as_view()),
]

skills = [
    url(r'^$', SkillList.as_view()),
    url(r'^(?P<id>[0-9]+)/$', SkillId.as_view()),
]

goals = [
    url(r'^$', GoalList.as_view()),
    url(r'^(?P<id>[0-9]+)/$', GoalId.as_view()),
    url(r'^(?P<goal_id>[0-9]+)/skills$', SkillInGoal.as_view()),
]

people = [
    url(r'^me/status', 'prototype.views.dictionaries.userStatus'),
    url(r'^(?P<id>[0-9]+)/status', 'prototype.views.dictionaries.userStatus'),
    url(r'^(?P<id>[0-9]+)/$', PersonId.as_view()),
    url(r'^(?P<id>[0-9]+)/locations$', PersonLocationListView.as_view(),name = 'PersonsLocation'),
    url(r'^(?P<id>[0-9]+)/goals/$', GoalsinPeople.as_view()),
    url(r'^(?P<person_id>[0-9]+)/goals/(?P<goal_id>[0-9]+)/candidates', GoalPartnersView.as_view()),
    url(r'^(?P<person_id>[0-9]+)/goals/(?P<goal_id>[0-9]+)', PersonGoalView.as_view())
]

reports = [
    url(r'^negative-reports', 'prototype.views.reports.negative_report'),
]

urlpatterns = [
    url(r'^api/characters/',include(characters)),
    url(r'^api/skills/',include(skills)),
    url(r'^api/goals/',include(goals)),
    url(r'^api/people/',include(people)),
    url(r'^api/reported/(?P<person_id>[0-9]+)/',include(reports)),
   # url(r'^', include('snippets.urls')),
    url(r'^auth/login/', 'django.contrib.auth.views.login', {'template_name':'admin/login.html'}, name='login'),
    url(r'^auth/', include('rest_framework_social_oauth2.urls')),
    url(r'^admin/', include(admin.site.urls)),
    #url(r'^api/', include('api.urls')),
    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),

]
