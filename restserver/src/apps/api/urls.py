
from django.conf.urls import patterns, include, url
from rest_framework import routers
from api import views

router = routers.DefaultRouter()
#router.register(r'users', views.UserViewSet)
#router.register(r'groups', views.GroupViewSet)

urlpatterns = patterns('',
    url(r'^', include(router.urls)),
    url(r'^1\.0/', include('api.v1_0.urls')),
    url('^versions/', 'api.views.get_versions'),
    url(r'^auth/', include('rest_framework.urls', namespace='rest_framework'))

)
