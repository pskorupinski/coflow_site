#
# from rest_framework import serializers
# from django.contrib.auth.models import User, Group
#
# class UserProfileSerializer(serializers.Serializer):
#     firstname = serializers.CharField(max_length=20, read_only=True)
#     lastname = serializers.CharField(max_length=20, read_only=True)
#     skills = serializers.ListField(child = serializers.CharField(max_length=20, read_only=True))
#     videoURL = serializers.URLField(allow_null=True)
#
# ###
#
# class UserSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = User
#         fields = ('url', 'username', 'email', 'groups')
#
# class GroupSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = Group
#         fields = ('url', 'name')
