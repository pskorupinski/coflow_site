#
# from rest_framework import status
# from rest_framework.response import Response
# from rest_framework.decorators import api_view, permission_classes
# from rest_framework import permissions
# from django.views.decorators.csrf import csrf_exempt
# from social.apps.django_app.utils import psa, load_strategy
# from rest_framework.permissions import AllowAny
# from rest_framework import viewsets
# from pprint import pprint
# from django.contrib.auth import login
#
#
# @psa('social:complete')
# def auth_by_token(request, backend):
#     print request.backend
#     backend = request.backend
#     user = request.user
#     user = backend.do_auth(
#         access_token=request.data.get('access_token'),
#         user=user.is_authenticated() and user or None
#         )
#     if user and user.is_active:
#         return user
#     else:
#         return None
#
#
# def auth(request):
#     auth_token = request.data.get('access_token', None)
#     backend = request.data.get('backend', None)
#     if auth_token and backend:
#         user = auth_by_token(request, backend)
#         return True, user
#     else:
#         return False, None
#
#
# @csrf_exempt
# @api_view(['POST'])
# @permission_classes((permissions.AllowAny,))
# def social_login(request):
#     print "social_signup"
#     print request.META
#     try:
#         istoken, user = auth(request)
#     except Exception, err:
#         print err
#         return Response(str(err), status=400)
#     if not istoken:
#         return Response("Bad request", status=400)
#     if user:
#         pprint(user)
#         #strategy = load_strategy(request=request)
#         #print strategy
#         login(request, user)
#         return Response( "User logged in", status=status.HTTP_200_OK )
#     else:
#         return Response("Bad Credentials", status=403)

# @api_view(["GET"])
# @permission_classes((AllowAny,))
# def get_versions(dummy_request):
#     """ returns list of available api versions """
#     serializer = VersioningSerializer(Versioning())
#     return Response(serializer.data, status=status.HTTP_200_OK)
#
# @api_view(["GET"])
# @permission_classes(())
# def get_user_profile(request,offset):
#     print request
#     id=int(offset)
#     if(id==999):
#         user = UserProfileData('A','B',['a','b'],'http://www.youtube.com/1')
#         serializer = UserProfileSerializer(user)
#         return Response(serializer.data, status=status.HTTP_200_OK)
#     else:
#         return Response(status=status.HTTP_204_NO_CONTENT)
#
#
# #############################
#
#
# class UserViewSet(viewsets.ModelViewSet):
#     """
#     API endpoint that allows users to be viewed or edited.
#     """
#     queryset = User.objects.all().order_by('-date_joined')
#     serializer_class = UserSerializer
#
#
# class GroupViewSet(viewsets.ModelViewSet):
#     """
#     API endpoint that allows groups to be viewed or edited.
#     """
#     queryset = Group.objects.all()
#     serializer_class = GroupSerializer