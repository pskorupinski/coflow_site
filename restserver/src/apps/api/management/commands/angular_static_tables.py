
import os
import codecs

from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils.translation import activate

from prototype.models import UserProfile, Language, Interest, Sport, Music, Religion, Politics, RelationshipStatus, \
    FieldOfWork, FieldOfStudy, EducationLevel, WorkStatus, MultiLookingFor
from matching.models import GENDER_CHOICES as LF_GENDER_CHOICES

from countries_base.models import Country


class Command(BaseCommand):
    """generates static tables with translations for angular"""
    help = 'generates static tables with translations for angular'

    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, **kwargs)
        self.static_tables_output = {}
        self.translations_outputs = {}
        for lang in settings.LANGUAGES:
            self.translations_outputs[lang[0]] = {}

    def _add_model_common(self, model):
        """ generates the static table for model """
        items = list(model.objects.all().order_by('id'))
        self.static_tables_output[model.__name__] = '{prefix:"%s",ids:[%s]}'%(model.__name__.upper(),
            ",".join(map(lambda x: str(x.id), items)))
        return items

    def _add_model(self, model):
        """ generates the static table with translation for model """
        items = self._add_model_common(model)
        for lang in settings.LANGUAGES:
            activate(lang[0])
            for item in items:
                self.translations_outputs[lang[0]]["%s_%d"%(model.__name__.upper(), item.id)] = item.get_name()

    def _add_choices(self, choices, name, prefix):
        """ generates the static table with translation for Choices """
        self.static_tables_output[name] = '{prefix:"%s",ids:[%s]}'%(prefix,
            ",".join(map(lambda x: str(x[0]) if x[0].__class__ == int else "'%s'"%x[0], choices)))
        for lang in settings.LANGUAGES:
            activate(lang[0])
            for choice in choices:
                self.translations_outputs[lang[0]]["%s_%s"%(prefix, str(choice[0]))] = unicode(choice[1])

    def _add_languages(self):
        items = list(Language.objects.all().order_by('order_en'))
        options = "{%s}"%(",".join(map(lambda x: "%d:'%s'"%(x.id, x.name), items)))
        self.static_tables_output['Language'] = '{ids:[%s],options:%s}'%(",".join(map(lambda x: str(x.id), items)),
            options)

    def _save(self):
        """ saves results to files """
        # save choices
        try:
            os.makedirs("angular_export")
        except:
            pass
        f = open("angular_export/static_tables.dump.js", "wt")
        out = "var STATIC_TABLES = {%s};\n"%",\n".join(map(lambda x: "%s:%s"%(x[0], x[1]),
            self.static_tables_output.items()))
        f.write(out)
        f.close()

        for lang in settings.LANGUAGES:
            try:
                os.makedirs("angular_export/%s"%lang[0])
            except:
                pass
            f = codecs.open("angular_export/%s/static.json"%lang[0], "wt", encoding="utf-8")
            out = u"{%s}\n"%u",".join(map(lambda x: u'"%s":"%s"'%(x[0], x[1]),
                self.translations_outputs[lang[0]].items()))
            f.write(out)
            f.close()

    def handle(self, *args, **options):
        self._add_model(Country)
        self._add_model(Religion)
        self._add_model(Politics)
        self._add_model(RelationshipStatus)
        self._add_model(Interest)
        self._add_model(Sport)
        self._add_model(Music)
        self._add_model(FieldOfStudy)
        self._add_model(FieldOfWork)
        self._add_model(EducationLevel)
        self._add_model(WorkStatus)
        self._add_choices(UserProfile.BODY_TYPE, "BodyType", "BODYTYPE")
        self._add_choices(UserProfile.ETHNICITY_CHOICES, "Ethnicity", "ETHNICITY")
        self._add_choices(UserProfile.SMOKES_CHOICES, "Smokes", "SMOKES")
        self._add_choices(UserProfile.GENDER_CHOICES, "Gender", "GENDER")
        self._add_choices(UserProfile.HEIGHT_CHOICES, "Height", "HEIGHT")
        self._add_choices(LF_GENDER_CHOICES, "LFGender", "LFGENDER")
        self._add_choices(MultiLookingFor.choices, "LookingFor", "LOOKINGFOR")
        self._add_choices(UserProfile.HAS_CHILDREN_CHOICES, "HasChildren", "HASCHILDREN")
        self._add_choices(UserProfile.WANTS_CHILDREN_CHOICES, "WantsChildren", "WANTSCHILDREN")
        self._add_languages()
        self._save()
