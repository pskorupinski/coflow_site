
from datetime import date, datetime

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from prototype.models import UserProfile

class Command(BaseCommand):
    help = 'sets up test data required by e2e tests'

    def handle(self, *args, **options):
        # test@test.pl - user account
        user,created = User.objects.get_or_create(email='test@test.pl')
        user.set_password('test1234')
        user.is_superuser=True
        user.is_staff=True
        user.save()
        profile,created = UserProfile.objects.get_or_create(user=user)
        profile.first_name = 'MyTest'
        profile.save()

