


from rest_framework import serializers


class Version(object):
    """ class for keeping versions """
    def __init__(self, number, url, is_deprecated):
        self.number = number
        self.url = url
        self.is_deprecated = is_deprecated


class Versioning(object):
    """ API versioning static class """
    _versions = []

    @classmethod
    def register(cls, number, is_deprecated):
        """ should be called while registering a new api version """
        for version in cls._versions:
            if version.number == number:
                raise Exception("Api %s version already registered"%number)
        version = Version(number, "/api/%s/"%number, is_deprecated)
        cls._versions.append(version)
        return version

    @property
    def versions(self):
        """ returns ordered list of API versions """
        return sorted(self._versions, key=lambda x: x.number)


class VersionSerializer(serializers.Serializer):
    """ serializer for a single version """
    number = serializers.CharField(max_length=20, read_only=True)
    url = serializers.CharField(max_length=20, read_only=True)
    is_deprecated = serializers.BooleanField(read_only=True)


class VersioningSerializer(serializers.Serializer):
    """ serializer for versioning """
    versions = VersionSerializer(many=True, read_only=True)


