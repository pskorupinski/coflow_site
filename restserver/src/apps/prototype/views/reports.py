from rest_framework import permissions,status
from rest_framework.decorators import api_view, permission_classes
from prototype.businesslogic.reports import *
from prototype.models import Person
from rest_framework.response import Response


@api_view(['POST'])
@permission_classes((permissions.AllowAny,))
def negative_report(request,person_id):
    reporting = Person.objects.get(id=1)
    reported = Person.objects.get(id=person_id)
    description = request.data['reporttext']
    report = Report(typology="negative_report", is_negative=True, description=description)
    manager = ReportsManager(reported=reported, reporting=reporting)
    if(manager.manage(report)):
        return Response(status=status.HTTP_201_CREATED)
