from django.shortcuts import render
from rest_framework import generics

from prototype.permissions import CollabPermission, ObjectIsVisible, CollabInGoalPermission, OwnerPermission, IsRegisteredNonBlocked
from prototype.serializers import *
from rest_framework import permissions,status
from django.http import Http404
from rest_framework.response import Response
from rest_framework.views import APIView
from prototype.permissions import *
from django.core import serializers
import pdb
from django.http import HttpResponse
from prototype.businesslogic.candidatelist.algorithms.partners_search import *

# Create your views here.


class SkillList(generics.ListCreateAPIView):
    model = Skill
    serializer_class = SkillSerializer
    permission_classes = [
        permissions.AllowAny
    ]
    queryset = Skill.objects.all()

    def get_queryset(self):
        request = self.request
        user = self.request.user
        return self.queryset


class GoalList(generics.ListCreateAPIView):
    model = Goal
    serializer_class = GoalSerializer
    permission_classes = [
        permissions.AllowAny
    ]
    queryset = Goal.objects.all()


class CharacterList(generics.ListCreateAPIView):
    model = CharacterSerializer
    serializer_class = CharacterSerializer
    permission_classes = [
        permissions.AllowAny
    ]
    queryset = CharacterStrength.objects.all()


class AddGoal(generics.CreateAPIView):
    model = Goal
    serializer_class = GoalSerializer
    permission_classes = [
        permissions.AllowAny
    ]
    # def perform_create(self,serializer_class):


class PersonView(generics.RetrieveAPIView):
    model = Person
    serializer_class = PersonGetSerializer
    permission_classes = [
        permissions.IsAuthenticated
    ]

    def retrieve(request, *args, **kwargs):
        pass


class CharacterId(generics.ListCreateAPIView):
    model = CharacterStrength
    serializer_class = CharacterSerializer
    permission_classes = [
        permissions.AllowAny
    ]

    def get_object(self,id):
        try:
            return CharacterStrength.objects.get(id=id)
        except CharacterStrength.DoesNotExist:
            raise Http404

    def get(self,request,id,format=None):
        obj = self.get_object(id)
        serializer = CharacterSerializer(obj)
        return Response(serializer.data)


class GoalId(generics.ListCreateAPIView):
    model = Goal
    serializer_class = GoalSerializer
    permission_classes = [
        permissions.AllowAny
    ]

    def get_object(self,id):
        try:
            return Goal.objects.get(id=id)
        except Goal.DoesNotExist:
            raise Http404

    def get(self,request,id,format=None):
        obj = self.get_object(id)
        serializer = GoalSerializer(obj)
        return Response(serializer.data)


class SkillId(generics.ListCreateAPIView):
    model = Skill
    serializer_class = SkillSerializer
    permission_classes = [
        permissions.AllowAny
    ]

    def get_object(self,id):
        try:
            return Skill.objects.get(id=id)
        except Skill.DoesNotExist:
            raise Http404

    def get(self,request,id,format=None):
        obj = self.get_object(id)
        serializer = SkillSerializer(obj)
        return Response(serializer.data)


class SkillInGoal(APIView):
    model = Goal
    serializer_class = SkillSerializer
    permission_classes = [
        permissions.AllowAny
    ]

    def get_object(self,goal_id):
        try:
            return Goal.objects.get(id=goal_id)
        except Goal.DoesNotExist:
            raise Http404

    def get(self, request, goal_id, format=None):
        obj = self.get_object(goal_id)
        serializer = SkillSerializer(obj.relevantSkills,many = True)
        return Response(serializer.data)



from rest_framework.decorators import detail_route,parser_classes
from rest_framework.parsers import FormParser,MultiPartParser
from rest_framework.permissions import AllowAny
from rest_framework.status import *


class PersonId(generics.ListCreateAPIView):
    model = Person
    serializer_class = PersonGetSerializer
    permission_classes = [
        permissions.AllowAny
    ]

    def get_object(self,id):
        try:
            return Person.objects.get(id=id)
        except Person.DoesNotExist:
            raise Http404

    def get(self,request,id,format=None):
        obj = self.get_object(id)
        serializer = PersonGetSerializer(obj,context=dict(request=request))
        return Response(serializer.data)

    def put(self,request,id,format=None):
        obj = self.get_object(id)
        user = request.user
        if user.id != id:
            return Response('U can edit only yout profile', status=400)
        else:
            serializer = PersonPutSerializer(obj, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

    # @detail_route(methods=['POST'], permission_classes=[AllowAny])
    # @parser_classes((FormParser, MultiPartParser,))
    # def image(self, request, *args, **kwargs):
    #     if 'upload' in request.data:
    #         person = self.get_object()
    #         person.photo.delete()
    #
    #         upload = request.data['upload']
    #
    #         person.photo.save(upload.name, upload)
    #
    #         return Response(status=HTTP_201_CREATED, headers={'Location': person.photo.url})
    #     else:
    #         return Response(status=HTTP_400_BAD_REQUEST)


class PersonLocationListView(generics.ListAPIView):
    model = Person
    serializers = PersonLocationSerializer
    permission_classes = [
        permissions.AllowAny
    ]
    queryset = Skill.objects.all()

from rest_framework.decorators import api_view, permission_classes
import urllib2
import json

def storePersonFacebookData(person):
    social_user = person.social_auth.filter(
        provider='facebook',
    ).first()
    if social_user:
        url = u'https://graph.facebook.com/{0}/' \
              u'friends?fields=id,name,location,picture' \
              u'&access_token={1}'.format(
                  social_user.uid,
                  social_user.extra_data['access_token'],
              )
        request = urllib2.Request(url)
        friends = json.loads(urllib2.urlopen(request).read()).get('data')
        for friend in friends:
            print friend
        return False


@api_view(['GET'])
@permission_classes((permissions.IsAuthenticated,))
def userStatus(request):
    if request.user.isregistered:
        return Response(True, status=200)
    else:
        if storePersonFacebookData(request.user):
            return Response(True, status=201)

    return Response(False, status=400)


class GoalsinPeople(APIView):
    model = Person
    serializer_class = PeopleIdGoalsSerializer
    permission_classes = [
        permissions.AllowAny
    ]

    def get_object(self, id):
        try:
            return Person.objects.get(id=id)
        except Goal.DoesNotExist:
            raise Http404

    def get(self, request, id, format=None):
        object = self.get_object(id)
        serializer = PeopleIdGoalsSerializer(object.goals, many=True, context={'person_id':id})
        return Response(serializer.data)


class PersonGoalView(generics.ListCreateAPIView,generics.CreateAPIView):
    model = PersonGoal
    serializer_class = PersonGoalSerializer
    permission_classes = [
        permissions.AllowAny
    ]
    my_permission_classes = [
        OwnerPermission(),
        ObjectIsVisible(),
        IsRegisteredNonBlocked()
    ]


    def get_object(self,person_id,goal_id):
        try:
            object = PersonGoal.objects.get(goal = goal_id,person = person_id)
            self.check_my_permissions(self.request, person_id, object)
            return object
        except Goal.DoesNotExist:
            raise Http404

    def get(self,request,person_id,goal_id,format=None):
        object = self.get_object(person_id, goal_id)
        serializer = PersonGoalSerializer(object)
        return Response(serializer.data)

    def put(self,request, person_id, goal_id, format=None):
        object = self.get_object(person_id,goal_id)
        serializer = PersonGoalSerializer(object, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,person_id,goal_id,format=None):
        object = self.get_object(person_id,goal_id)
        object.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def post(self,request,person_id,goal_id,format=None):
        object = PersonGoalSerializer(data=request.data)
        if object.is_valid():
            goal = Goal.objects.get(id=goal_id)
            person = Person.objects.get(id=person_id)
            object.save(goal=goal, person=person)
            return Response(object.data)
        return Response(object.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_my_permissions(self):
        return self.my_permission_classes



    def check_my_permissions(self, request, person_id, object):
            if not self.my_permission_classes[0].has_permission(request, person_id):
                self.permission_denied(
                    request, message=getattr(self.my_permission_classes[0], 'message', None)
                )
            if not self.my_permission_classes[1].has_permission(request, object):
                self.permission_denied(
                    request, message=getattr(self.my_permission_classes[1], 'message', None)
                )

            if not self.my_permission_classes[2].has_permission(request):
                self.permission_denied(
                    request, message=getattr(self.my_permission_classes[2], 'message', None)
                )

class GoalPartnersView(APIView):
    permission_classes = [
        permissions.AllowAny
    ]

    def get(self, request, person_id, goal_id):
        partners_generator = GoalPartnersListGenerator(DummySearchStrategy())
        person = Person.objects.get(pk=person_id)
        goal = Goal.objects.get(pk=goal_id)
        serializer = PersonGetSerializer(partners_generator.search(person, goal))
        return Response(serializer.data)

