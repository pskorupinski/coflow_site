from rest_framework import permissions
from prototype.models import *

class CollabInGoalPermission(permissions.BasePermission):
    """
    Chcecks if user who sent request is in collaboration in a particular person goal
    """
    message = 'You are not in collaboration in PersonalGoal specified in URL, permission denied.'

    def has_permission(self, request, object):
        collaborations = object.persongoalcollaboration_set.all()
        collaborators = []
        for collab in collaborations:
            collaborators = collaborators + [collab.person]
        for collaborator in collaborators:
            if collaborator.pk == request.user.pk:
                return True
        return False


class CollabPermission(permissions.BasePermission):
    """
    Checks if user who sent request is in ANY collaboration with user specified in URL
    """
    message = 'You are not in any collaboration with Person specified in URL, permission denied.'

    def has_permission(self, request, person_id):
        collaborators = []
        for personGoal in Person.objects.get(pk=person_id).persongoal_set.all():
            for collab in personGoal.persongoalcollaboration_set.all():
                collaborators = collaborators + [collab.person]
        for collaborator in collaborators:
            if collaborator.pk == request.user.pk:
                return True
            else:        #if the user who sends request is the owner
                return False           #the same here



class OwnerPermission(permissions.BasePermission):
    """
    Checks if user who sent request is the same as user specified in URL
    """
    message = 'You are not the owner, permission denied.'

    def has_permission(self, request, person_id):
            return request.user.pk == person_id


class ObjectIsVisible(permissions.BasePermission):
    """
    Checks if requested object is visible
    """
    message = 'Object is not visible, permission denied.'

    def has_permission(self, request, obj):
        if obj is None:
            return True

        if obj.visibilityLevel == 1:
            return True
        else:
            return False

class IsRegisteredNonBlocked(permissions.BasePermission):
    """
    Checks if Person is registered and non blocked
    """
    def has_permission(self, request):
        person = Person.objects.get(pk=request.user.pk)
        if person.isblocked == True:
            self.message = 'Person is blocked, permission denied.'
            return False
        if person.isregistered == False:
            self.message = 'Person is not registered, permission denied'
            return False

        return True