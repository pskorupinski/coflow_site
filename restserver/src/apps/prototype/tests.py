from django.contrib.auth.models import AnonymousUser, User
from rest_framework.test import APITestCase, APIRequestFactory
from prototype.views.dictionaries import *
from rest_framework.test import force_authenticate

# Create your tests here.
class PermissionsTest(APITestCase):
    def setUp(self):
        self.factory = APIRequestFactory()

    def check_owner_permission(self, id):
        self.setUp()
        request = self.factory.get('people/'+str(id), data=None, Format='JSON', Authorization = 'Bearer facebook CAAB8CquSZB7cBAKtZACH5u7O3jJ42HCzwZARWiKdY74D4bzTKg82tIZBZCsmrre3AwNZB4hH5bbpxk85WDi4JZCq7gsLaKCaMTuqYZBQulMHRRc72PR2f2sOBi9yPU7UZB6VcOXDz2rgShOGqHYlME9OfRYJQ9qISRQDPk9Gaj07KWS2fmgdJrjly6kDCZAL3JD0AZD')
        request.user = AnonymousUser()
        ###
        request.user.pk = 2 #should be assigned during authentication, modify to act as different users
        force_authenticate(request, user=request.user)
        ###
        response = PersonId.as_view()(request, id=id)
        print response.data

    def check_collaborator_permission(self, person_id, goal_id):
        self.setUp()
        request = self.factory.get('/api/people/'+str(person_id)+'/goals/'+str(goal_id), data=None, Format='JSON', Authorization = 'Bearer facebook CAAB8CquSZB7cBAKtZACH5u7O3jJ42HCzwZARWiKdY74D4bzTKg82tIZBZCsmrre3AwNZB4hH5bbpxk85WDi4JZCq7gsLaKCaMTuqYZBQulMHRRc72PR2f2sOBi9yPU7UZB6VcOXDz2rgShOGqHYlME9OfRYJQ9qISRQDPk9Gaj07KWS2fmgdJrjly6kDCZAL3JD0AZD')
        request.user = AnonymousUser()
        ###
        request.user.pk = 2 #should be assigned during authentication, modify to act as different users
        force_authenticate(request, user=request.user)
        ###
        response = PersonGoalView.as_view()(request, person_id=person_id, goal_id=goal_id)
        print response.data