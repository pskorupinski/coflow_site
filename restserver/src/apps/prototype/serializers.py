from rest_framework import serializers
from prototype.models import *
from prototype.views import *
import pdb


class CharacterSerializer(serializers.ModelSerializer):

    class Meta:
        model = CharacterStrength
        fields = ('id','name')


class GoalSerializer(serializers.ModelSerializer):

    class Meta:
        model = Goal
        fields = ('id','name')


class SkillSerializer(serializers.ModelSerializer):

    class Meta:
        model = Skill
        fields = ('id', 'name')


class PersonGetSerializer(serializers.ModelSerializer):
    personid = serializers.ReadOnlyField(source='id')
    locations = serializers.CharField(source='locationname', required=False, allow_blank=True)
    contact = serializers.CharField(source='contacthandle', required=False,allow_blank=True)
    class Meta:
        model = Person
        fields = ('personid', 'firstname', 'lastname', 'birthdate', 'photo','weeklyhours', 'locations', 'goals',
                  'person_skills','contact')

class PersonPutSerializer(serializers.ModelSerializer):

    class Meta:
        model = Person
        fields = ('firstname', 'lastname', 'birthdate', 'photo', 'weeklyhours' )

class PersonLocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Person
        fields = ('locationX','locationY','locationname')


class PersonSkillSerializer(serializers.ModelSerializer):

    class Meta:
        model = PersonSkill

class PersonEvaluationSerializer(serializers.ModelSerializer):

    class Meta:
        model = PersonEvaluation

class PersonGoalSerializer(serializers.ModelSerializer):
    personid = serializers.SerializerMethodField('get_person_id')
    goalid = serializers.SerializerMethodField('get_goal_id')
    what = serializers.SerializerMethodField('get_person_what')
    why = serializers.SerializerMethodField('get_person_why')
    deadline = serializers.SerializerMethodField('get_person_deadline')
    visibility = serializers.SerializerMethodField('get_person_visibility')
    bckimage = serializers.SerializerMethodField('get_person_bckimage')
    collaborators = serializers.SerializerMethodField('get_collaborats')

    def get_person_id(self, obj):
        return obj.person.id

    def get_goal_id(self, obj):
        return obj.goal.id

    def get_person_goal(self,obj):
        person = Person.objects.get(id=obj.person.id)
        goal = Goal.objects.get(id=obj.goal.id)
        return PersonGoal.objects.get(person=person, goal=goal)

    def get_person_why(self, obj):
        personid = obj.person.id
        person_goal = self.get_person_goal(obj)
        return person_goal.why

    def get_person_what(self, obj):
        personid = obj.person.id
        person = self.get_person_goal(obj)
        return person.name

    def get_person_deadline(self, obj):
        personid = obj.person.id
        person_goal = self.get_person_goal(obj)
        person_task = PersonGoalTask.objects.get(personGoal=person_goal)
        return person_task.deadline

    def get_person_visibility(self, obj):
        personid = obj.person.id
        person_goal = self.get_person_goal(obj)
        person_task = PersonGoalTask.objects.get(personGoal=person_goal)
        return True

    def get_person_bckimage(self, obj):
        personid = obj.person.id
        person_goal = self.get_person_goal(obj)
        return 'null'

    def get_collaborats(self, obj):
        personid = obj.person.id
        person_goal = self.get_person_goal(obj)
        return 'null'

    class Meta:
        model = PersonGoal
        fields = ('personid', 'goalid', 'what', 'why', 'deadline', 'visibility', 'bckimage', 'collaborators')

class PersonGoalCollaborationSerializer(serializers.ModelSerializer):

    class Meta:
        model = PersonGoalCollaboration

class NegativeReportsSerializer(serializers.ModelSerializer):

    class Meta:
        model = NegativeReport

class UpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Update

class PersonGoalTaskSerializer(serializers.ModelSerializer):

    class Meta:
        model = PersonGoalTask

class PeopleIdGoalsSerializer(serializers.ModelSerializer):
    personid = serializers.SerializerMethodField('get_person_id')
    goalid = serializers.ReadOnlyField(source='id')
    what = serializers.SerializerMethodField('get_person_what')
    why = serializers.SerializerMethodField('get_person_why')
    deadline = serializers.SerializerMethodField('get_person_deadline')
    visibility = serializers.SerializerMethodField('get_person_visibility')
    bckimage = serializers.SerializerMethodField('get_person_bckimage')
    collaborators = serializers.SerializerMethodField('get_collaborats')

    def get_person_id(self, obj):
        return int(self.context.get("person_id"))

    def get_person_goal(self, personid, goalid):
        return PersonGoal.objects.get(person=personid, goal=goalid)

    def get_person_why(self, obj):
        personid = int(self.context.get("person_id"))
        person_goal = self.get_person_goal(personid, obj.id)
        return person_goal.why

    def get_person_what(self, obj):
        personid = int(self.context.get("person_id"))
        person = self.get_person_goal(personid, obj.id)
        return person.name

    def get_person_deadline(self, obj):
        personid = int(self.context.get("person_id"))
        person_goal = self.get_person_goal(personid, obj.id)
        person_task = PersonGoalTask.objects.get(personGoal=person_goal)
        return person_task.deadline

    def get_person_visibility(self, obj):
        personid = int(self.context.get("person_id"))
        person_goal = self.get_person_goal(personid, obj.id)
        person_task = PersonGoalTask.objects.get(personGoal=person_goal)
        return True

    def get_person_bckimage(self, obj):
        personid = int(self.context.get("person_id"))
        person_goal = self.get_person_goal(personid, obj.id)
        return 'null'

    def get_collaborats(self, obj):
        personid = int(self.context.get("person_id"))
        person_goal = self.get_person_goal(personid, obj.id)
        return 'null'

    class Meta:
        model = Goal
        fields = ('personid', 'goalid', 'what', 'why', 'deadline', 'visibility', 'bckimage', 'collaborators')

