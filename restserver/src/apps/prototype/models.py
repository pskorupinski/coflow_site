from django.db import models
from django.db.models import BooleanField
from django.contrib.auth.models import *
from prototype.modelmanager import *
from prototype.businesslogic.observers.abstract import AbstractObservee


class Person(AbstractBaseUser):
    """
    Basic info about Person
    """
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['firstname','lastname']
    email = models.EmailField(unique=True)
    username = models.CharField(max_length=30)
    is_admin = models.BooleanField(default=False)

    firstname = models.CharField(max_length=30)
    lastname = models.CharField(max_length=30)
    birthdate=models.DateField(null=True,blank=True)
    videourl = models.URLField(null=True,blank=True)
    photo = models.ImageField(null=True,blank=True, upload_to='api/photos/')
    bio = models.CharField(max_length=30,null=True,blank=True)

    locationX = models.FloatField(null=True,blank=True)
    locationY = models.FloatField(null=True,blank=True)
    locationname = models.CharField(max_length = 50,null=True,blank=True)

    contacthandle = models.CharField(max_length = 50,null=True,blank=True)
    contacttype = models.CharField(max_length = 50,null=True,blank=True)

    weeklyhours = models.IntegerField(null=True,blank=True)

    isregistered = models.BooleanField(default=False)
    isblocked = models.BooleanField(default=False)

    objects = PersonManager()

    goals = models.ManyToManyField('Goal', null=True, blank=True)
    # skills = models.ManyToManyField('PersonSkill', null=True,blank=True)

    def get_full_name(self):
        return self.firstname + self.lastname

    def get_short_name(self):
        return self.firstname + self.lastname

    @property
    def is_superuser(self):
        return self.is_admin

    @property
    def is_staff(self):
        return self.is_admin

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return self.is_admin

    def __unicode__(self):
        return u'%s (%s)' %(self.username, self.email)


class Skill(models.Model):
    """
    Skill description
    """
    name = models.CharField(max_length=30)

    def __unicode__(self):
        return self.name


class PersonSkill(models.Model):
    """
    Info about Person's skill
    """
    person = models.ForeignKey(Person, related_name='person_skills')
    skill = models.ForeignKey(Skill)
    skillLevel = models.SmallIntegerField()


class CharacterStrength(models.Model):
    """
    Character strength description
    """
    name = models.CharField(max_length=30)

    def __unicode__(self):
        return self.name


class PersonEvaluation(models.Model):
    """
    Information who submitted evaluation, whose strengths were evaluated
    """
    submitter = models.ForeignKey(Person,related_name='submitter')
    evaluated = models.ForeignKey(Person,related_name='evaluated')
    characterStrengths = models.ManyToManyField(CharacterStrength)


class Goal(models.Model):
    """
    Contains information about Goal
    """
    name = models.CharField(max_length=30)
    relevantSkills=models.ManyToManyField(Skill)

    def __unicode__(self):
        return self.name


class PersonGoal(models.Model):
    """
    Contains information about Person's Goal
    """
    goal = models.ForeignKey(Goal)
    person = models.ForeignKey(Person)
    name = models.CharField(max_length=30)
    why = models.CharField(max_length=30)
    bckgImage = models.ImageField()
    visibilityLevel = models.SmallIntegerField()

    #collaborat = models.ManyToOneRel('PersonGoalCollaboration', null=True, blank=True)
    #interestingSkills=lista(Skill)


class PersonGoalCollaboration(models.Model):
    person = models.OneToOneField(Person)
    personGoal = models.ForeignKey(PersonGoal)
    collaboratorSkill = models.ForeignKey(PersonSkill)
    collRole = models.CharField(max_length=30)
    isActive = BooleanField()
    isActiveTimestamp = models.DateTimeField(auto_now_add=True)
    isConfirmed = models.BooleanField()


class NegativeReport(models.Model):
    """
    Contains information about a report, reported person, and reporting person
    """
    reportingPerson = models.ForeignKey(Person, related_name='reportingPerson')
    reportedPerson = models.ForeignKey(Person, related_name='reportedPerson')
    report = models.CharField(max_length=200)
    timestamp = models.DateTimeField(auto_now_add=True)


class Update(models.Model):
    recipient = models.ForeignKey(Person)
    annotatedText = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now_add=True)


class PersonGoalTask(models.Model):
    personGoal = models.ForeignKey(PersonGoal)
    name = models.CharField(max_length=100)
    createdBy = models.ForeignKey(Person)
    created = models.DateTimeField(auto_now_add=True)
    deadline = models.DateTimeField(auto_now_add=True)
    isDone = models.BooleanField()



