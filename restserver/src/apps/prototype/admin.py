from prototype.models import *
from django.contrib import admin

admin.site.register(Update)
admin.site.register(CharacterStrength)
admin.site.register(Goal)
admin.site.register(NegativeReport)
admin.site.register(Person)
admin.site.register(PersonGoal)
admin.site.register(PersonEvaluation)
admin.site.register(PersonGoalCollaboration)
admin.site.register(PersonSkill)
admin.site.register(PersonGoalTask)
admin.site.register(Skill)