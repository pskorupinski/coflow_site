from prototype.businesslogic.observers.abstract import AbstractObservee
from prototype.businesslogic.observers.observers import MaliciousEventsObserver


class Report(AbstractObservee):
    """
    Class representing any report in the system
    """

    def init(self):
        AbstractObservee.__init__(self)
        self.register(MaliciousEventsObserver())

    def __init__(self, typology, is_negative, *args, **kwargs):

        self.init()
        self.typology = typology
        self.is_negative = is_negative
        self.kwargs = kwargs


class ReportsManager:

    def __init__(self, reported, reporting):
        self.reported  = reported
        self.reporting = reporting
        pass

    def manage(self,report):
        self.handle_malicious(report)
        self.store(report)
        return True

    def handle_malicious(self,report):
        if report.is_negative:
            report.update_observers(person=self.reported,typology=report.typology,**report.kwargs)

    def store(self,report):
        print "storing... :)"
        # TODO store in database based on typology
