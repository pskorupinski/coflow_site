from prototype.businesslogic.observers.abstract import AbstractObserver


class MaliciousEventsObserver(AbstractObserver):

    def update(self, person, typology, description=None):
        """
        Check immediately whether person who is a subject of malicious event has some malicious history
        E.g. set person as blocked
        :return:
        """
        print "Caught malicious event:"
        print "Person:", person
        print "Typology:", typology
        print "Description:", description
        # TODO
