from abc import ABCMeta, abstractmethod


class AbstractObservee:

    def __init__(self):
        self.observers = []

    def register(self, observer):
        if not observer in self.observers:
            self.observers.append(observer)

    def unregister(self, observer):
        if observer in self.observers:
            self.observers.remove(observer)

    def unregister_all(self):
        if self.observers:
            del self.observers[:]

    def update_observers(self, *args, **kwargs):
        for observer in self.observers:
            observer.update(*args, **kwargs)

    def notify_observer(self,observer):
        pass


class AbstractObserver:
    __metaclass__ = ABCMeta

    def __init__(self):
        pass

    @abstractmethod
    def update(self, *args, **kwargs):
        """
        Update method that needs to be implemented by each inheriting class
        :param args:
        :param kwargs:
        :return:
        """
        pass