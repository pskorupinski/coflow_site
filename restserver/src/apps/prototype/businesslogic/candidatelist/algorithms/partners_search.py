from prototype.models import *

class GoalPartnersSearchStrategy:
    pass

class AISearchStrategy(GoalPartnersSearchStrategy):
    def find(self, localization, goal):
        pass

class DummySearchStrategy(GoalPartnersSearchStrategy):
    def find(self, location, goal):
        return Person.objects.filter(person_skills__skill__in = goal.relevantSkills.all(), locationname = location)

class GoalPartnersListGenerator:
    def __init__(self, strategy):
        self.how_to = strategy

    def search(self, localization, goal):
        self.how_to.find(localization, goal)
class Konrad:
    pass