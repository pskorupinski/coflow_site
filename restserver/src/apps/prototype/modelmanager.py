from django.contrib.auth.models import BaseUserManager


class PersonManager(BaseUserManager):

    def create_user(self, username, email=None, password=None, firstname=None, lastname=None, **extra_fields):
        if not email:
            email = username+"@unknown.com"
        if not firstname:
            firstname = username
        if not lastname:
            lastname = username
            #raise ValueError('Users must have an email address')

        user = self.model(
            email       = self.normalize_email(email),
            username    = username,
            firstname   = firstname,
            lastname    = lastname,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, firstname, lastname, email, password):
        user                = self.create_user(firstname+lastname, email, password, firstname, lastname)
        user.isregistered   = True
        user.is_admin       = True
        user.save(using=self._db)
        return user