import logging
import traceback
import sys


from django.http import Http404
from django.conf import settings

logger = logging.getLogger(__name__)

class ExceptionMiddleware:
    '''
    Put an 500 error even if there is settings.DEBUG = True

    good for test server
    '''
    def process_exception(self, request, exception):
        if isinstance(exception, Http404):
            return
        logger = logging.getLogger('django.request')
        if settings.DEBUG:
            print 'Internal Server Error: %s' % request.path
            print exception
            traceback.print_exc()

            #logger.error('Internal Server Error: %s' % request.path,
            #        exc_info=sys.exc_info(),
            #        extra={
            #            'status_code': 500,
            #            'request':request
            #    }
            #)

    def process_response(self, request, response):
        if response.status_code == 500:
            traceback.print_exc()
        return response